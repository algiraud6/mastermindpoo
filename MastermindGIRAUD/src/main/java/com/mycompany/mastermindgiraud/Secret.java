/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mastermindgiraud;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Acer
 */
public class Secret extends Combinaison{
    
    /**
     *
     * @param difficulte
     */
    public Secret(Difficulte difficulte){
        super(difficulte);
        
        ArrayList<Integer> nombresTires = new ArrayList<Integer>();
        Random gnpa = new Random();
        // on remplit une liste des nombres qu'on a le droit de tirer
        for(int i = 0; i < difficulte.getNbCouleurs(); i++) {
            nombresTires.add(i+1);
        }
        // On tire des nombres dans l'ensemble créé et on le supprime 
        // a la selection si les doublons sont interdits
        for (int i = 0; i < tailleCombinaison; i++) {
            int index = gnpa.nextInt(nombresTires.size());
            listPions[i] = new Pion(nombresTires.get(index));
            
            if(!difficulte.isDoublonAutorise()){
                nombresTires.remove(index);
            }             
        }   
    }

    Resultat checkTentative(Tentative tentativeActuelle, Difficulte difficulte) {
        Resultat resultat = new Resultat();
        Pion[] listePionsTentative = tentativeActuelle.listPions;
        int[] drapeaux = new int[tailleCombinaison];
        for (int i = 0; i < tailleCombinaison; i++) {
            drapeaux[i] = 0;
        }
        
        int nbRouges = 0;
        int nbBlancs = 0;
        
        // boucle de vérification des couleurs bien placées
        for (int i = 0; i < tailleCombinaison; i++) {
            if(tentativeActuelle.listPions[i].getCouleur() == listPions[i].getCouleur()){
                nbRouges++;
                resultat.indicePinsRouge.add(i);
                drapeaux[i]=1;
            }
        }
        
        //boucle de verification des couleurs mal placees
        for (int i = 0; i < tailleCombinaison; i++) {
            Pion pionTentative = listePionsTentative[i];
            for(int j = 0; j < tailleCombinaison; j++){
                Pion pionSecret = this.listPions[j];
                if(drapeaux[i]==0 && pionTentative.getCouleur()== pionSecret.getCouleur()){
                    nbBlancs++;
                    drapeaux[i]=1;
                    resultat.indicePinsBlanc.add(i);
                    break;
                }
            }
        }
        
        
        resultat.setNbPinsBlancs(nbBlancs);
        resultat.setNbPinsRouges(nbRouges);
        return resultat;
    }
    
    
}
