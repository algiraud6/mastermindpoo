/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mastermindgiraud;

/**
 *
 * @author Acer
 */
public class Joueur {
    
    /**
     *
     * @param nom
     */
    public Joueur(String nom){
        this.nom = nom;
    }
    
    private int score = 0;
    private String nom = "Joueur";

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }
    
    /**
     *
     * @return
     */
    public int getScore() {
        return score;
    }

    void setScore(int i) {
        this.score = i;
    }
    void incrementScore(int i){
        this.score += i;
    }
    
    
}
