/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mastermindgiraud;

import java.util.Arrays;

/**
 *
 * @author Acer
 */
abstract class Combinaison {

    @Override
    public String toString() {
        String sortie = "";
        for(int i = 0; i < listPions.length; i++){
           sortie = sortie+listPions[i].getCouleur();
        }
        
        return sortie;
    }
    
    
    public Combinaison(Difficulte difficulte) {
        this.tailleCombinaison = difficulte.getTailleCombinaison();
        this.listPions = new Pion[tailleCombinaison];

    }
    
    Pion[] listPions ;
    protected final int tailleCombinaison;
    
    /**
     * Cette fonction renvoie false si check ne contient pas de doublons
     * @param String check 
     */
    public static boolean checkDoublon(int [] check){
        return Arrays.stream(check).distinct().count() != check.length;
    }
    /**
     * Cette fonction renvoie false si check ne correspond pas aux 
     * règles de difficultés fournies
     * @param String check 
     */
    public static boolean check(Difficulte difficulte, int check){
        boolean flag = true;
        
        int tailleCombi = difficulte.getTailleCombinaison();
        int[] tabCheck = parseEntier(check);
        
        
        // On regarde que la tentative fasse bien la bonne taille
        if (tabCheck.length != tailleCombi) flag = false;
        
        //Si necessaire on vérifie l'absence de doublons
        if(!difficulte.isDoublonAutorise()){
            if(checkDoublon(tabCheck)) flag=false;
        }
        // On s'assure que chaque charactère soit autorisé
        for(int i = 0; i < tabCheck.length;i++){
            if(tabCheck[i] == 0 && tabCheck[i] > difficulte.getNbCouleurs())flag = false;
        }
        // On libère la mémoire de tabCheck
        tabCheck = null;
    return flag;
    }
    
    protected static int[] parseEntier(int entier){
        // On recupere la taille de l'entier en comptants le nb de caractères
        // le composant
        int tailleEntier = Integer.toString(entier).length();
        int[] tab = new int[tailleEntier];
        
        // On parse l'entier dans un tableau dimensionné
        for (int i = tab.length-1; i > -1; i--) {
            tab[i] = entier%10;
            entier = entier-tab[i];
            entier = entier/10;
        }
        
        return tab;
    }
    
    
}
