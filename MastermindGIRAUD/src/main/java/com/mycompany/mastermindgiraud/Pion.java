/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mastermindgiraud;

/**
 *
 * @author Acer
 */
public class Pion {

    
    private enum Couleurs{
        NOIR, //"\033[0;30m"
        ROUGE, //"\033[0;31m"
        VERT, //"\033[0;32m"
        JAUNE,//"\033[0;33m"
        BLEU,//"\033[0;34m"
        VIOLET,//"\033[0;35m"
        CYAN,//"\033[0;36m"
        BLANC,//"\033[0;37m"
        NULL,
    }
    
    private int couleur;

    /**
     *
     * @return
     */
    public int getCouleur() {
        return couleur;
    }
    
    Couleurs getNomCouleur() {
        int i = 0;
        for(Couleurs c : Couleurs.values()){
            if(i == this.couleur)
            {
                return c;
            }
        }
        return Couleurs.NULL;
    }
    
    
    /**
     *
     * @param couleur
     */
    public Pion(int couleur) {
        this.couleur = couleur;
    }
    
    
}

