/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mastermindgiraud;
import java.io.Serializable;

/**
 *
 * @author Acer
 */
public class Plateau{

    /**
     *
     */
    public Tentative[] essaisPrecedents;
    private Secret secret;

    /**
     *
     * @return
     */
    public Secret getSecret() {
        return secret;
    }
    
    /**
     *
     * @param difficulte
     */
    public Plateau(Difficulte difficulte) {
        this.essaisPrecedents = new Tentative[difficulte.getNbEssaisMax()];
        this.secret = new Secret(difficulte);
    }
    
    
    
    
}
