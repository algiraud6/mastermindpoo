/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mastermindgiraud;


import java.util.Scanner;


/**
 *
 * @author Acer
 */

public class Partie{

    /**
     *
     * @param difficulte
     * @param joueurActuel
     */
    public Partie(Difficulte difficulte, Joueur joueurActuel) {
        this.difficulte = difficulte;
        this.plateau = new Plateau(this.difficulte);
    }
    
    /**
     *
     * @param difCharge
     * @param joueurCharge
     * @param plateauCharge
     */
    public Partie(Difficulte difCharge,Joueur joueurCharge,Plateau plateauCharge){
        
    }
    
    private Difficulte difficulte;
    private Plateau plateau;
    private int depart;
  
    /**
     *Cette fonction lance une partie de mastermind 
     * 
     * @return int le score fait dans la partie, une défaite vaut -5
     */

    public int lancePartie(){
        Scanner sc = new Scanner(System.in);
        int inputUser;
        Tentative tentativeActuelle;
        Secret secretPartie = plateau.getSecret();
        Resultat resultatEssai;
        
        for (int i = this.depart; i < difficulte.getNbEssaisMax(); i++) {
            System.out.println("Saisir une tentative \n");
            inputUser = sc.nextInt();
            while(Combinaison.check(difficulte,inputUser) == false){
                System.out.println("Chiffre max autorisé : "+ difficulte.getTailleCombinaison());
                System.out.println("Votre saisie est erronée; Rappel de la taille"
                        + " nécessaire "+ difficulte.getTailleCombinaison()
                        +" doublon autorisé :"+ difficulte.isDoublonAutorise());
                inputUser = sc.nextInt();
            }
            tentativeActuelle = new Tentative(difficulte,inputUser);
            plateau.essaisPrecedents[i] = tentativeActuelle;
            
            resultatEssai = secretPartie.checkTentative(tentativeActuelle,difficulte);
            
            resultatEssai.afficheResultat(difficulte);
            afficherTableau();
            if(resultatEssai.getNbPinRouges() == secretPartie.tailleCombinaison){
                return (difficulte.getNbEssaisMax()-i-1);
            }
            

        }
        return -5;
    }
    
    /**
     *
     */
    public void afficherTableau(){
        for (int i = 0; i < plateau.essaisPrecedents.length; i++) {
            if(plateau.essaisPrecedents[i]!= null){
                System.out.println("Tentative n°"+(i+1)+" "+plateau.essaisPrecedents[i].toString());
            }
            
        
            
        } 
    }
}
