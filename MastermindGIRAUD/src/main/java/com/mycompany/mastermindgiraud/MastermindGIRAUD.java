/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.mastermindgiraud;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Acer
 */
public class MastermindGIRAUD {

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Difficulte difficulte;
        Joueur joueur1 = new Joueur("Marius");
        int choixUser = -1;
        System.out.println("Courage aux joueurs lançant ce programme !");
        
        // Choix de la difficulté
        System.out.println("Choisissez une difficulté \n 1 - Facile \n 2 - Difficile\n3 - Personnalisé");
        choixUser = sc.nextInt();
        switch (choixUser) {
            case 1:
                difficulte = new Difficulte(12, 6, 4, true,false);
                break;
            case 2:
                difficulte = new Difficulte(10, 8, 5, false,true);
                break;
            case 3:
                int nbTenta,nbCouleurs,tailleCombi;
                boolean indice,doublonAutorise;
                
                System.out.println("Combien de tentatives ?");
                nbTenta = sc.nextInt();
                System.out.println("Combien de couleurs ? de 6 a 8");
                nbCouleurs = sc.nextInt();
                System.out.println("Combien de couleurs par combinaison ? minimum 2");
                tailleCombi = sc.nextInt();
                System.out.println("Voulez vous des indices ? true = oui");
                indice = sc.nextBoolean();
                System.out.println("Voulez vous autoriser les doublons ? true = oui");
                doublonAutorise = sc.nextBoolean();
                
                difficulte = new Difficulte(nbTenta,nbCouleurs,tailleCombi,indice,doublonAutorise);
                break;
            default:
                System.out.println("Choix Erroné, difficulté maximale choisie");
                difficulte = new Difficulte(10, 8, 5, false,true);
                break;        
            }
        System.out.println("Saisissez un mode de jeu \n 1 - Mode solo`\n 2 - Mode multijoueur \n");
        choixUser = -1;
        while(choixUser<1 || choixUser >2){
            choixUser = sc.nextInt();
        }    
        if(choixUser == 1){
            Partie partie = new Partie(difficulte, joueur1);
            // Lance une partie et inscrit le score dans le profil du joueur
            joueur1.setScore(partie.lancePartie());
            
        }
        else{ 
            PartieMulti pm = new PartieMulti(difficulte);
            pm.lanceBO();
            
        }
    }
}
