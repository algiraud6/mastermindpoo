/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mastermindgiraud;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Acer
 */
public class PartieMulti {
        
    Partie partieActuelle;
    Difficulte difficulte;
    ArrayList<Joueur> listeJoueurs = new ArrayList<>();
    int nbManches;
    int nbJoueurs;
    int depart = 0;

    /**
     *
     * @param difficulte
     */
    public PartieMulti(Difficulte difficulte){
        Scanner sc = new Scanner(System.in);
        System.out.println("Choisissez le nombre manches pour la partie");            
        nbManches = sc.nextInt();
        System.out.println("Choisissez le nombre de joueurs");            
        nbJoueurs = sc.nextInt();
        for (int i = 0; i < nbJoueurs; i++) {
                System.out.println("Saisir le nom du joueur "+(i+1));  
                String nomJoueur = sc.next();
                listeJoueurs.add(new Joueur(nomJoueur));
            }
        this.nbManches = nbManches;
        this.nbJoueurs = nbJoueurs;
        this.difficulte = difficulte;
    }
    
    /**
     *
     */
    public void lanceBO(){
        // Boucle de plusieurs manches 
        for (int i = depart; i < this.nbManches; i++) {
            System.out.println("DEBUT DE LA MANCHE N°"+(i+1));
            for (Joueur joueurActuel : listeJoueurs) {
                System.out.println("C'est au tour de : "+joueurActuel.getNom()+" !");
                partieActuelle = new Partie(this.difficulte, joueurActuel);
                joueurActuel.incrementScore(partieActuelle.lancePartie());
                System.out.println("Score Actuel "+joueurActuel.getScore());
            }
        }
            
        System.out.println("SCORES FINAUX : ");
        for (Joueur joueur : listeJoueurs) {
            System.out.println(joueur.getNom()+" a un total de "+joueur.getScore()+" points !!");
        }
    }
}
