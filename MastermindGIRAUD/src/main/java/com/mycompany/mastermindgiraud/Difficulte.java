/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mastermindgiraud;

/**
 *
 * @author Acer
 */
public class Difficulte {

    /**
     *
     * @param nbEssaisMax
     * @param nbCouleurs
     * @param tailleCombinaison
     * @param modeIndice
     * @param doublonA
     */
    public Difficulte(int nbEssaisMax, int nbCouleurs, int tailleCombinaison, boolean modeIndice, boolean doublonA) {
        this.nbEssaisMax = nbEssaisMax;
        this.nbCouleurs = nbCouleurs;
        this.tailleCombinaison = tailleCombinaison;
        this.modeIndice = modeIndice;
        this.doublonAutorise = doublonA;
    }
    
    private int nbEssaisMax;
    private int nbCouleurs;
    private int tailleCombinaison;
    private boolean modeIndice;
    private boolean doublonAutorise;

    /**
     *
     * @return
     */
    public boolean isDoublonAutorise() {
        return doublonAutorise;
    }
    
    /**
     *
     * @return
     */
    public int getNbEssaisMax() {
        return nbEssaisMax;
    }

    /**
     *
     * @return
     */
    public int getNbCouleurs() {
        return nbCouleurs;
    }

    /**
     *
     * @return
     */
    public int getTailleCombinaison() {
        return tailleCombinaison;
    }

    /**
     *
     * @return
     */
    public boolean getModeIndice() {
        return modeIndice;
    }
   


    
    
    
}
