package com.mycompany.mastermindgiraud;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

import java.util.Iterator;
/**
 *
 * @author Acer
 */
public class Tentative extends Combinaison {
    
    /**
     *
     * @param saisieUser
     * @param nbCouleurs
     * @param tailleCombi
     * @return
     */
    public static boolean check(String saisieUser, int nbCouleurs , int tailleCombi){
        if(saisieUser.length() != tailleCombi){
            return false;
        }
        for (int i = 0; i < saisieUser.length(); i++) {
            char c = saisieUser.charAt(i);
            if(c >= nbCouleurs);
            return false;
        }
        return true;
    }

    /**
     *
     * @param difficulte
     * @param init
     */
    public Tentative(Difficulte difficulte, int init) {
        super(difficulte);
        int[] tabInit = parseEntier(init);
        for (int i = 0; i < tabInit.length; i++) {
            this.listPions[i] = new Pion(tabInit[i]);
        }
        
        
    }
    
}
