/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mastermindgiraud;

import java.util.ArrayList;

/**
 *
 * @author Acer
 */
public class Resultat {
    
    /**
     *
     */
    public Resultat(){
        this.indicePinsBlanc = new ArrayList<>();
        this.indicePinsRouge = new ArrayList<>();
    }
    
    
    private int nbPinsRouges;
    private int nbPinsBlancs;
    ArrayList<Integer> indicePinsRouge,indicePinsBlanc;

    /**
     *
     * @return
     */
    public int getNbPinRouges() {
        
        return nbPinsRouges;
    }

    /**
     *
     * @return
     */
    public int getNbPinsBlancs() {
        return nbPinsBlancs;
    }

    /**
     *
     * @param nbPinsRouges
     */
    public void setNbPinsRouges(int nbPinsRouges) {
        this.nbPinsRouges = nbPinsRouges;
    }

    /**
     *
     * @param nbPinsBlancs
     */
    public void setNbPinsBlancs(int nbPinsBlancs) {
        this.nbPinsBlancs = nbPinsBlancs;
    }
    
    /**
     *
     * @param difficulte
     */
    public void afficheResultat(Difficulte difficulte){
        
        System.out.println("Nombre de pins rouges : "+this.nbPinsRouges);
        if(difficulte.getModeIndice()){
            for(int e:indicePinsRouge){
                System.out.println("Pion bien placé en : "+(e+1)+"°eme position");
            }
        }
        System.out.println("Nombre de pins blancs : "+this.nbPinsBlancs);
        if(difficulte.getModeIndice()){
            for(int e:indicePinsBlanc){
                System.out.println("Pion de bonne couleur mal placé en : "+(e+1)+"°eme position");
            }
            
        }
        
    }
    
   
}
